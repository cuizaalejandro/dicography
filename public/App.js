class App{
    constructor(){

        this.loadAlbums();
    }

    loadAlbums(){

       const promise =  fetch('album.json')

       
       promise.then(this.onAlbumsRecieved)
              .then(this.onJsonReady)
              .catch(this.onError);
    }

    onAlbumsRecieved(response){
        
        return response.json();
        alert('recibido');
    }

    onError(error){
        alert('Error'+ error);
    }

    onJsonReady(json){

        const container =  document.querySelector("#album");

        for(const album of json){
            new Album(album,container);
        }


    }
}